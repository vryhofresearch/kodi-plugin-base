import sys


def ask_question(**kwargs):
    answer = None
    question = kwargs.get('question')
    prompt = kwargs.get('prompt', '> ')

    print(question)

    if sys.version_info == 3:
        answer = input(prompt)

    else:
        answer = raw_input(prompt)

    return answer

questions = {
    'app_name': 'App Name',
    'app_identifier': 'App Identifier (plugin.plugintype.yourplugin)',
    'app_version': 'App Version',
    'app_developer': 'Developer Name',
    'app_developer_email': 'Developer Email Address',
    'plugin_provides': 'What does your plugin provide? (videos, audio)',

}

details_link = 'http://kodi.wiki/view/Addon.xml'

