# KODI-PLUGIN-BASE #

Do not use this code verbatim.  Make sure everything in it is replaced, as it is a copy of another plugin that is GPL Licensed.

### What is this repository for? ###

This repo is to be used as a learning/starting point for your own Kodi Plugin.  It is simple enough to work with that anyone with some Python knowledge should be able to start from it, and create their own plugin.
